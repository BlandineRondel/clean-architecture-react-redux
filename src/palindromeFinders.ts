export const palindromeFinders = (sentence: string) => {
    const loweredSentence = sentence.toLowerCase();
    let palindromes: string[] = [];
    let isPalindrome = true;
    if (loweredSentence.length <= 1)
        return palindromes;
    for (let i = 0; i < loweredSentence.length / 2; i++) {
        if (areSymmetricCharactersDifferent(loweredSentence, i)) {
            isPalindrome = false;
            break;
        }
    }
    if (isPalindrome)
        palindromes.push(sentence);
    return palindromes;
}

const areSymmetricCharactersDifferent = (sentence: string, i: number) => {
    return sentence.charAt(i) !== sentence.charAt(sentence.length - 1 - i);
}
