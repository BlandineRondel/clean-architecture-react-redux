import {combineReducers} from "redux";
import {Word} from "../appState.interface";
import * as actionCreators from "../../corelogic/usecases/words-retrieval/actionCreators";

const data = (state: Word[] = [], action: actionCreators.Actions) => {
    switch (action.type) {
        case 'WORDS_RETRIEVED':
            return action.payload.words;
        default:
            return state;
    }
}

const fetching = (state: boolean = false, action: actionCreators.Actions) => {
    switch (action.type) {
        case 'RETRIEVE_WORDS':
            return true;
        case 'WORDS_RETRIEVED':
            return false;
        default:
            return state;
    }
};

// transforme le state avec les nouvelles données
export const wordsRetrievalReducer = combineReducers({
    data,
    fetching
});
