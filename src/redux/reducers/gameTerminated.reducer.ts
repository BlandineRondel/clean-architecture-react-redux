import * as actionCreators from "../../corelogic/usecases/game-termination/actionCreators";

export const gameTerminatedReducer = (state: boolean = false, action: actionCreators.Actions) => {
    switch (action.type) {
        case 'TERMINATE_GAME':
            return true;
        default:
            return state;
    }
}
