const correction = (state: { [key: string]: boolean } = {}, action: any) => {
    switch (action.type) {
        case 'ANSWER_VALID':
            return {
                ...state,
                [action.payload]: true
            };
        case 'ANSWER_INVALID':
            return {
                ...state,
                [action.payload]: false
            };
        default:
            return state;
    }
}

export default correction