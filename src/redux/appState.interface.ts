// STORE REDUX (Le schéma du state global de toute l'appli !)
export interface AppState {
    wordsRetrieval: {
        data: Word[];
        fetching: boolean;
    },
    correction: {[key: string]: boolean},
    gameTerminated: boolean;
}

//DOMAIN MODEL (OUHHHH!!!! à éviter)
export interface Word {
    id: string;
    label: string;
}
