import {FunctionComponent, useEffect, useState} from "react";

interface Props {
    seconds: number;
    whenElapsed: () => void;
}

export const CountDown: FunctionComponent<Props> = ({seconds, whenElapsed}) => {

    const [timer, setTimer] = useState(seconds);

    useEffect(() => {
        let interval: any;
        if(timer === 0) {
            whenElapsed();
            return () => clearInterval(interval);
        }
        interval = setInterval(() => {
            setTimer(timer - 1);
        }, 1000);
        return () => clearInterval(interval);
    }, [timer]);

    return <div data-testid='count-down'>
        <h1>{timer}</h1>
    </div>;
};
