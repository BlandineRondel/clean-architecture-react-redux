import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../../../redux/appState.interface";
import {useEffect} from "react";
import {retrieveWords} from "../../../corelogic/usecases/words-retrieval/retrieveWords";
import {Col, Row} from "react-bootstrap";
import {WordRow} from "./wordRow.component";
import {getScore} from "../view-models-generators/correctionVMGenerator";
import {CountDown} from "./CountDown.component";
import {getEndGame} from "../view-models-generators/endGameVMGenerator";
import * as actionCreators from "../../../corelogic/usecases/game-termination/actionCreators";

// Our first primary adapter in production
export const WordsGame = () => {

    const dispatch = useDispatch();
    const words = useSelector((state: AppState) => state.wordsRetrieval.data);
    const score = useSelector(getScore);
    const endGame = useSelector(getEndGame);

    useEffect(() => {
        dispatch(retrieveWords);
    }, [dispatch]);

    const whenCountDownElapsed = () => {
        dispatch(actionCreators.Actions.terminateGame());
    };

    return <Row className="justify-content-center">
        <Col md={2}>
            <CountDown seconds={20} whenElapsed={whenCountDownElapsed}/>
            <h2>Score : {score}</h2>
            {endGame.isEndGame ? <>
                <h4>{endGame.message}</h4>
            </> : <ul>
                {
                    words.map((word) =>
                        <WordRow key={word.id} word={word}/>)
                }
            </ul>}
        </Col>
    </Row>
}
