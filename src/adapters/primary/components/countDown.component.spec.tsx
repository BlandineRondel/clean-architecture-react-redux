import {act, render, screen} from "@testing-library/react";
import React from "react";
import {CountDown} from "./CountDown.component";
import {Provider} from "react-redux";
import {configureStore, ReduxStore} from "../../../redux/configureStore";

describe('Countdown component', () => {

    let store: ReduxStore;

    beforeEach(() => {
        store = configureStore({});
    });

    beforeEach(() => {
        jest.useFakeTimers();
    });

    it('should start with a given time', () => {
        renderCountDown(20, 0, () => {
            expect(screen.getByTestId('count-down')).toHaveTextContent('20');
        });
    });

    it('should decrease timer by one each second', () => {
        renderCountDown(20, 1, () => {
            expect(screen.getByTestId('count-down')).toHaveTextContent('19');
        });
    });

    it('should stop timer at 0 second', () => {
        renderCountDown(0, 1, () => {
            expect(screen.getByTestId('count-down')).toHaveTextContent('0');
        });
    });

    it('stop the game when countdown is elapsed', () => {
        let triggeredAction = false;
        renderCountDown(20, 20,
            () => expect(triggeredAction).toBeTruthy(),
            () => triggeredAction = true);
    });

    const renderCountDown = (seconds: number,
                             currentSeconds: number,
                             expectations: () => void,
                             whenElapsed = () => {
                             }) => {
        const {unmount} = render(
            <CountDown seconds={seconds} whenElapsed={whenElapsed}/>
        );
        act((): any => jest.advanceTimersByTime(currentSeconds * 1000));
        expectations();
        unmount();
    }

    afterEach(() => {
        jest.runOnlyPendingTimers();
        jest.useRealTimers();
    });

});
