import {Button} from "react-bootstrap";
import {FunctionComponent, useState} from "react";
import {Word} from "../../../redux/appState.interface";
import {useDispatch, useSelector} from "react-redux";
import validAnswer from "../../../corelogic/usecases/answer-validation/validAnswer";
import {getAnswerCorrection} from "../view-models-generators/correctionVMGenerator";

interface Props {
    word: Word;
}

export const WordRow: FunctionComponent<Props> = ({word}) => {

    const [answer, setAnswer] = useState<number | null>(null);
    const dispatch = useDispatch();

    const correction = useSelector(getAnswerCorrection(word.id));

    const getAnswer = (event: any) => {
        setAnswer(+event.target.value);
    }

    const submitAnswer = () => {
        if (answer !== null)
            dispatch(validAnswer(word.id, answer));
    }

    return <li>
        {word.label}
        <input name={"answer" + word.id} onChange={getAnswer}/>
        <Button onClick={submitAnswer}>Valider</Button>
        <span>{correction}</span>
    </li>;
};




