import {AppState} from "../../../redux/appState.interface";

export const getEndGame = ((state: AppState) => {
    return {isEndGame: state.gameTerminated, message: 'Terminé !!!'}
});
