import {getAnswerCorrection} from "./correctionVMGenerator";
import {configureStore, ReduxStore} from "../../../redux/configureStore";

describe('Correction view model generator', () => {

    let store: ReduxStore;

    beforeEach(() => {
        store = configureStore({});
    });

    it('should inform for an invalid answer', () => {
       store.dispatch({type: 'ANSWER_INVALID', payload: '123abc'})
        expectAnswerCorrection('Échoué')
    });

    it('should inform for a valid answer', () => {
        store.dispatch({type: 'ANSWER_VALID', payload: '123abc'})
        expectAnswerCorrection("Réussi");
    });

    it('should not inform for any correction before having one answer', () => {
        expectAnswerCorrection("À faire");
    });

    const expectAnswerCorrection = (expectedCorrection: string) => {
        expect(getAnswerCorrection('123abc')(store.getState())).toEqual(expectedCorrection);
    }

});
