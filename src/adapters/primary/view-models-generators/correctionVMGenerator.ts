import {AppState} from "../../../redux/appState.interface";

export const getAnswerCorrection = (wordId: string) => (state: AppState) => {
    const correction = state.correction[wordId];
    if (typeof (correction) === 'undefined')
        return "À faire";
    return correction ? "Réussi" : "Échoué";
};

export const getScore = (state: AppState) => {
    return Object.values(state.correction).reduce((acc, b) => {
        return acc + (b ? 1 : 0)
    }, 0) + " points";
}
