import {WordsGateway} from "../../../corelogic/gateways/wordsGateway.interface";
import {Word} from "../../../redux/appState.interface";

// Our real secondary adapter (GO BACKEND!!!)
export class HttpWordsGateway implements WordsGateway {

    retrieve(): Promise<Word[]> {
        return Promise.resolve([{id: '123abc', label: 'ANNA_FROM_BACKEND'}]);
    }

    validAnswer(): Promise<boolean> {
        return Promise.resolve(true)
    }
}
