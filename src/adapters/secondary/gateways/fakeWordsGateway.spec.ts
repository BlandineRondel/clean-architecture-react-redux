import {SmartFakeWordsGateway} from "./smartFakeWordsGateway";

describe('Fake words gateway',() => {

    it('should valid a good answer', async () => {
        const wordsGateway = new SmartFakeWordsGateway();
        wordsGateway.feed([{id: "123abc", label: "anna"}]);
        expect(await wordsGateway.validAnswer("123abc", 1)).toBe(true);
    });

    it('should valid a good answer', async () => {
        const wordsGateway = new SmartFakeWordsGateway();
        wordsGateway.feed([{id: "123abc", label: "annab"}]);
        expect(await wordsGateway.validAnswer("123abc", 1)).toBe(false);
    });


});
