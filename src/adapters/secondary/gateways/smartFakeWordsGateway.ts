import {InMemoryWordsGateway} from './inMemoryWordsGateway';
import {palindromeFinders} from '../../../palindromeFinders';

// SECONDARY ADAPTER (FAKE!!!) - UTILISABLE EN PROD !!
export class SmartFakeWordsGateway extends InMemoryWordsGateway {

    async validAnswer(wordId: string, answer: number): Promise<boolean> {
        const word = this.words.find(({id}) => wordId === id)
        if (!word)
            return false;
        const finderResult = palindromeFinders(word.label)
        return finderResult.length === answer
    }
}
