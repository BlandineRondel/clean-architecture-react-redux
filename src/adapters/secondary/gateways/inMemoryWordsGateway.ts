import {WordsGateway} from "../../../corelogic/gateways/wordsGateway.interface";
import {Word} from "../../../redux/appState.interface";

// SECONDARY ADAPTER (FAKE!!!) - UTILISABLE EN PROD !!
export class InMemoryWordsGateway implements WordsGateway {

    // FAKE DE BACKEND DATA (c'est comme un carton avec des données)
    private _words: Word[] = [];

    private _isNextAnswerInvalid = false;
    public lastAnswerValidation = {};

    retrieve(): Promise<Word[]> {
        return Promise.resolve(this._words);
    }

    validAnswer(wordId:string, answer:number): Promise<boolean> {
        this.lastAnswerValidation = {wordId, answer}
        return Promise.resolve(!this._isNextAnswerInvalid)
    }

    // METHODE SECRETE !!! ABSENTE DE L'INTERFACE !
    feed(words: Word[]) {
        this._words = words;
    }

    considerNextAnswerAsInvalid() {
        this._isNextAnswerInvalid = true;
    }


    get words(): Word[] {
        return this._words;
    }
}
