// Anna est ici => [Anna, ici]
// J'ai Paul dans le radar,
// il est avec Anna => [Anna, radar]

import {palindromeFinders} from "./palindromeFinders";

describe('Palindromes finder', () => {

    describe('Empty sentence', () => {

        it('should NOT find any palindrome', () => {
            expectPalindromes('')();
        });
    });

    describe('Non-empty sentence', () => {

        describe('Single-word sentence', () => {

            it('should check whether the word is a  palindrome', () => {
                expectPalindromes('aa')('aa');
                expectPalindromes('à')();
                expectPalindromes('ab')();
                expectPalindromes('aab')();
                expectPalindromes('aaba')();
                expectPalindromes('abba')('abba');
                expectPalindromes('kbayack')();
                expectPalindromes('Anna')('Anna');
            });

        });

    });

    const expectPalindromes = (sentence: string) => (...expectedFoundPalindromes: string[]) => {
        expect(palindromeFinders(sentence)).toEqual(expectedFoundPalindromes);
    };

});
