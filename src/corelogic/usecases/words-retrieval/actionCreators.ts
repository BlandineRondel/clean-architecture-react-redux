import {ActionsUnion, createAction} from "../../../redux/customAction";
import {Word} from "../../../redux/appState.interface";

export const Actions = {
    retrieveWords: () => createAction('RETRIEVE_WORDS'),
    wordsRetrieved: (words: Word[]) => createAction('WORDS_RETRIEVED', {words}),
};

export type Actions = ActionsUnion<typeof Actions>;
