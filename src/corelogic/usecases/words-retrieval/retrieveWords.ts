import {ThunkResult} from "../../../redux/configureStore";
import * as actionCreators from "./actionCreators";

// USE CASE (BUSINESS LOGIC) => dispatch des actions en fonction des résultats obtenus
export const retrieveWords: ThunkResult<void> =
    async (dispatch, getState, {wordsGateway}) => {
    dispatch(actionCreators.Actions.retrieveWords());
    const words = await wordsGateway.retrieve();
    dispatch(actionCreators.Actions.wordsRetrieved(words));
}


