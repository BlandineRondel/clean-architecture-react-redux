import {configureStore, ReduxStore} from "../../../redux/configureStore";
import {retrieveWords} from "./retrieveWords";
import {InMemoryWordsGateway} from "../../../adapters/secondary/gateways/inMemoryWordsGateway";
import {AppState} from "../../../redux/appState.interface";

describe('Words retrieval', () => {

    //REDUXSTORE is a shortcut for Store<AppState> with special dispatch accepting FUNCTION!
    let store: ReduxStore;
    let wordsGateway: InMemoryWordsGateway;
    let initialState: AppState;

    beforeEach(() => {
        wordsGateway = new InMemoryWordsGateway();
        store = configureStore({wordsGateway});
        initialState = store.getState();
    });

    it('should track the process of words retrieval', (done) => {
        const unsubscribe = store.subscribe(() => {
            expect(store.getState()).toEqual({
                ...initialState,
                wordsRetrieval: {
                    data: [],
                    fetching: true
                }
            });
            unsubscribe();
            done();
        });
        store.dispatch(retrieveWords);
    });

    it('should not retrieve any words when no one exists', async () => {
        await store.dispatch(retrieveWords);
        expect(store.getState()).toEqual({
            ...initialState,
            wordsRetrieval: {
                data: [],
                fetching: false
            }
        });
    });

    it('should retrieve existing words', async () => {
        wordsGateway.feed([{id: '123abc', label: 'Kayak'}, {id: '456def', label: 'Anna'}]);
        // AWAIT IMPORTANT => THINK EVENT LOOP (PROMISE !)
        await store.dispatch(retrieveWords);
        expect(store.getState()).toEqual({
            ...initialState,
            wordsRetrieval: {
                data: [{id: '123abc', label: 'Kayak'}, {id: '456def', label: 'Anna'}],
                fetching: false
            }
        });
    });

});
