import {ActionsUnion, createAction} from "../../../redux/customAction";
import {Word} from "../../../redux/appState.interface";

export const Actions = {
    terminateGame: () => createAction('TERMINATE_GAME'),
};

export type Actions = ActionsUnion<typeof Actions>;
