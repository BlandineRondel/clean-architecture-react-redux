import {configureStore, ReduxStore} from "../../../redux/configureStore";
import {AppState} from "../../../redux/appState.interface";
import * as actionCreators from "./actionCreators";

describe('Words retrieval', () => {

    //REDUXSTORE is a shortcut for Store<AppState> with special dispatch accepting FUNCTION!
    let store: ReduxStore;
    let initialState: AppState;

    beforeEach(() => {
        store = configureStore({});
        initialState = store.getState();
    });

    it('should not terminate the game initially', async () => {
        expect(store.getState()).toEqual({
            ...initialState,
            gameTerminated: false
        });
    });

    it('should terminate the game', async () => {
        await store.dispatch(actionCreators.Actions.terminateGame());
        expect(store.getState()).toEqual({
            ...initialState,
            gameTerminated: true
        });
    });

});
