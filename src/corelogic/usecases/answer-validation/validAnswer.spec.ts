import { configureStore, ReduxStore } from '../../../redux/configureStore';
import { InMemoryWordsGateway } from '../../../adapters/secondary/gateways/inMemoryWordsGateway';
import { AppState } from '../../../redux/appState.interface';
import validAnswer from './validAnswer';

describe('Answer validation', () => {

  let store: ReduxStore;
  let wordsGateway: InMemoryWordsGateway;
  let initialState: AppState;

  beforeEach(() => {
    wordsGateway = new InMemoryWordsGateway();
    store = configureStore({wordsGateway});
    initialState = store.getState();
  });

  it('should valid a good answer', async () => {
    const wordId = 'abc123';
    const answer = 1;
    await store.dispatch(validAnswer(wordId, answer))

    expect(store.getState()).toEqual({
      ...initialState,
      correction: {
        'abc123': true
      }
    })
    expect(wordsGateway.lastAnswerValidation).toEqual({wordId, answer})
  });

  it('should not valid a bad answer', async () => {
    const wordId = 'abc123';
    const answer = 1;
    wordsGateway.considerNextAnswerAsInvalid();
    await store.dispatch(validAnswer(wordId, answer))

    expect(store.getState()).toEqual({
      ...initialState,
      correction: {
        'abc123': false
      }
    })

    expect(wordsGateway.lastAnswerValidation).toEqual({wordId, answer})
  });

});
