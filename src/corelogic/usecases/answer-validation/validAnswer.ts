import {ThunkResult} from '../../../redux/configureStore';

const validAnswer = (wordId: string, answer: number): ThunkResult<Promise<void>> => async (dispatch: any, getState, {wordsGateway}) => {
    const isValid = await wordsGateway.validAnswer(wordId, answer);
    dispatch({type: isValid ? 'ANSWER_VALID' : 'ANSWER_INVALID', payload: wordId})
}

export default validAnswer;
