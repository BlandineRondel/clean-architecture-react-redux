import {Word} from "../../redux/appState.interface";


export interface WordsGateway {
    // SECONDARY PORT !!!  Frontière entre les adapters et le coeur
    // Les implems ne se trouvent SURTOUT PAS dans corelogic mais dans "adapters"!
    retrieve(): Promise<Word[]>;

    validAnswer(wordId: string, answer: number): Promise<boolean>;
}
