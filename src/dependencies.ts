import {WordsGateway} from "./corelogic/gateways/wordsGateway.interface";

export interface Dependencies {
    wordsGateway: WordsGateway
}
