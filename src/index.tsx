import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import {configureStore} from "./redux/configureStore";
import {SmartFakeWordsGateway} from './adapters/secondary/gateways/smartFakeWordsGateway';

// BOOSTRAPER !!
// C'est ici qu'on choisit les bons adapters secondaires !

const wordsGateway = new SmartFakeWordsGateway();
wordsGateway.feed([
    {id: '123abc', label: 'kayak'},
    {id: '456def', label: 'Anna'},
    {id: '789def', label: 'radar'},
    {id: '999def', label: 'Metro'}
]);
// INIT STORE WITH DEPENDENCIES!
const store = configureStore({wordsGateway});

ReactDOM.render(
    <React.StrictMode>
        {/* ALLOW COMPONENT TO REACH THE STORE */}
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

