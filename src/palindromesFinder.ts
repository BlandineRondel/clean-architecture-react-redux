export const PalindromesFinder = (sentence: string) => {
  if(!sentence) {
    return [];
  }
  const arraySentence = sentence.split('');

  let isPalindrome = true;
  const sentenceLength = arraySentence.length;
  arraySentence?.forEach((letter, i) => {
    if (arraySentence[sentenceLength - i] !== letter) {
      isPalindrome = false
    }
  });

  if(isPalindrome) {
    return [sentence]
  }
  return []

}